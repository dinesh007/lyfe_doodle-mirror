package com.lyfe.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.lyfe.LyfeApplication;
import com.lyfe.R;
import com.lyfe.adapter.ViewPagerAdapter;
import com.lyfe.fragments.SharedByMeFragment;
import com.lyfe.fragments.SharedWithMeFragment;


public class MainActivity extends BaseActivity {

    public static final String TAG = "AndroidDrawing";
    private static MainActivity currentRef;

    public Firebase mRef;
    public Firebase mBoardsRef;
    private Firebase mSegmentsRef;
    //private SharedDoodleInfoAdapter<HashMap> mBoardListAdapter;
    private ValueEventListener mConnectedListener;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public FloatingActionButton fabCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    private void initComponents() {
        /**
         * FireBase references
         * */
        mRef = LyfeApplication.getInstance().getFireBaseReference();
        mBoardsRef = mRef.child("boardmetas");
        mBoardsRef.keepSynced(true); // keep the board list in sync
        mSegmentsRef = mRef.child("boardsegments");

        fabCreate = (FloatingActionButton) findViewById(R.id.btnCreate);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }


    @Override
    protected void onStart() {
        super.onStart();

        // Set up a notification to let us know when we're connected or disconnected from the Firebase servers
        mConnectedListener = mRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    //Toast.makeText(MainActivity.this, "Connected to Firebase", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(MainActivity.this, "Disconnected from Firebase", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // No-op
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SharedByMeFragment(), "Shared by me");
        adapter.addFragment(new SharedWithMeFragment(), "Shared with me");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    fabCreate.show();
                } else {
                    fabCreate.hide();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}