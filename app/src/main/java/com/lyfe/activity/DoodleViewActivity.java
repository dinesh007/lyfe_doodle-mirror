package com.lyfe.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.lyfe.LyfeApplication;
import com.lyfe.R;
import com.lyfe.utils.StaticData;
import com.lyfe.utils.view.DoodleContextMenu;
import com.lyfe.utils.view.DoodleContextMenuManager;

import java.io.IOException;
import java.util.Map;


public class DoodleViewActivity extends BaseDrawerActivity implements DoodleContextMenu.OnDoodleContextMenuItemClickListener, View.OnClickListener {

    ImageButton btnMore;
    private String doodleKey;
    private ImageView imgDoodle;
    private Firebase mFireBaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doodle_view);
        getBundleData();
        initComponents();
        setOnClickListeners();
        setDoodleView();
    }

    private void setOnClickListeners() {
        btnMore.setOnClickListener(this);
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("doodle_key")) {
                doodleKey = bundle.getString("doodle_key");
            }
        }
    }

    private void initComponents() {
        mFireBaseRef = LyfeApplication.getInstance().getFireBaseReference();
        btnMore = (ImageButton) findViewById(R.id.btnMore);

        imgDoodle = (ImageView) findViewById(R.id.ivFeedCenter);
    }

    private void setDoodleView() {
        mFireBaseRef.child(StaticData.BOARDMETAS).child(doodleKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Map<String, Object> boardValues = (Map<String, Object>) dataSnapshot.getValue();
                if (boardValues != null && boardValues.get(StaticData.WIDTH) != null && boardValues.get(StaticData.HEIGHT) != null) {
                    try {
                        imgDoodle.setImageBitmap(DoodleEditActivity.decodeFromBase64(boardValues.get("thumbnail").toString()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    public void onMoreClick() {
        DoodleContextMenuManager.getInstance().toggleContextMenuFromView(findViewById(R.id.btnMore), 0, this);
    }

    @Override
    public void onReportClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onBlockCreatorClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onUnFollowClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onCancelClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnMore:
                onMoreClick();
                break;
            default:
                break;
        }
    }
}