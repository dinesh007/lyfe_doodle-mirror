package com.lyfe.activity;

import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;

import com.lyfe.R;

/**
 * Created by SURESH on 28.10.15.
 */
public class BaseActivity extends AppCompatActivity {

    ImageButton ivBtnMenu;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initComponents();
    }

    private void initComponents() {
        ivBtnMenu = (ImageButton) findViewById(R.id.ivBtnMenu);
    }


    public void setContentViewWithoutInject(int layoutResId) {
        super.setContentView(layoutResId);
    }

    protected void setupToolbar() {
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//            toolbar.setNavigationIcon(R.drawable.ic_menu_white);
//        }
    }

    public ImageButton getMenuButton() {
        return ivBtnMenu;
    }
}
