package com.lyfe.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;
import com.firebase.client.ValueEventListener;
import com.lyfe.LyfeApplication;
import com.lyfe.R;
import com.lyfe.models.NewDoodleModel;
import com.lyfe.utils.CanvasView;
import com.lyfe.utils.PopUtils;
import com.lyfe.utils.Segment;
import com.lyfe.utils.StaticData;
import com.lyfe.utils.StaticUtils;
import com.lyfe.utils.SyncedBoardManager;
import com.lyfe.utils.view.DoodleEditContextMenu;
import com.lyfe.utils.view.DoodleEditContextMenuManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class DoodleEditActivity extends Activity implements DoodleEditContextMenu.OnDoodleEditContextMenuItemClickListener, View.OnClickListener {

    public static final int THUMBNAIL_SIZE = 256;

    private ImageButton btnMore;
    private CanvasView canvasView;
    private LinearLayout llUndo, llRedo, llClear;
    private Firebase mFireBaseRef, mMetadataRef, mSegmentsRef, mBoardsRef;
    int selectedThicknessVallue = 1;
    private Context mContext;


    private ValueEventListener mConnectionListener;
    private int mBoardWidth;
    private int mBoardHeight;
    private boolean isCreateBoard;
    private String doodleKey;
    private NewDoodleModel newDoodleModel;
    public static Bitmap backgrounImageBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doodle_edit);
        mContext = this;
        getBundleData();
        initComponents();
        setClickListner();
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("doodle_key")) {
                doodleKey = bundle.getString("doodle_key");
                isCreateBoard = bundle.getBoolean("is_create", false);
            } else {
                newDoodleModel = (NewDoodleModel) bundle.getSerializable("doodle_data");
                isCreateBoard = bundle.getBoolean("is_create", false);
            }
        }
    }


    private void initComponents() {
        btnMore = (ImageButton) findViewById(R.id.btnMore);
        canvasView = (CanvasView) findViewById(R.id.ivDoodleCreater);
        llUndo = (LinearLayout) findViewById(R.id.ll_undu);
        llRedo = (LinearLayout) findViewById(R.id.ll_redu);
        llClear = (LinearLayout) findViewById(R.id.ll_clear);

        mFireBaseRef = LyfeApplication.getInstance().getFireBaseReference();
        mBoardsRef = mFireBaseRef.child(StaticData.BOARDMETAS);
        mBoardsRef.keepSynced(true); // keep the board list in sync
        mSegmentsRef = mFireBaseRef.child(StaticData.BOARDSEGMENTS);
        SyncedBoardManager.restoreSyncedBoards(mSegmentsRef);
        backgrounImageBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.background);
    }

    private void setClickListner() {
        btnMore.setOnClickListener(this);
        llUndo.setOnClickListener(this);
        llRedo.setOnClickListener(this);
        llClear.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Set up a notification to let us know when we're connected or disconnected from the Firebase servers
        mConnectionListener = mFireBaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    Toast.makeText(DoodleEditActivity.this, "Connected to Firebase", Toast.LENGTH_SHORT).show();
                    if (isCreateBoard) {
                        createBoard();
                    } else {
                        openDoodleBoard(doodleKey);
                    }
                } else {
                    Toast.makeText(DoodleEditActivity.this, "Disconnected from Firebase", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    private void createBoard() {
        // create a new board
        final Firebase newBoardRef = mBoardsRef.push();
        Map<String, Object> newBoardValues = new HashMap<>();
        android.graphics.Point size = new android.graphics.Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        newBoardValues.put(StaticData.CREATED_AT, ServerValue.TIMESTAMP);
        newBoardValues.put(StaticData.CREATED_BY, newDoodleModel.getName());
        newBoardValues.put(StaticData.WIDTH, newDoodleModel.getSize());
        newBoardValues.put(StaticData.HEIGHT, newDoodleModel.getSize());
        newBoardRef.setValue(newBoardValues, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase ref) {
                if (firebaseError != null) {
                    Log.e("Exp in creation board", firebaseError.toString());
                    throw firebaseError.toException();
                } else {
                    // once the board is created, start a DrawingActivity on it
                    openDoodleBoard(newBoardRef.getKey());
                }
            }
        });
    }

    public void openDoodleBoard(final String key) {
//        mFireBaseRef = new Firebase(StaticData.FIRE_BASE_URL);
//        mFireBaseRef = ref;
        mMetadataRef = mFireBaseRef.child(StaticData.BOARDMETAS).child(key);
        mSegmentsRef = mFireBaseRef.child(StaticData.BOARDSEGMENTS).child(key);
        mMetadataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Map<String, Object> boardValues = (Map<String, Object>) dataSnapshot.getValue();
                if (boardValues != null && boardValues.get(StaticData.WIDTH) != null && boardValues.get(StaticData.HEIGHT) != null) {
                    mBoardWidth = ((Long) boardValues.get(StaticData.WIDTH)).intValue();
                    mBoardHeight = ((Long) boardValues.get(StaticData.HEIGHT)).intValue();

//                    Segment segment = dataSnapshot.getValue(Segment.class);
//                    getCanvas().drawSegment(segment, CanvasView.paintFromColor(segment.getColor()));
//                    // Tell the view to redraw itself
//                    getCanvas().invalidate();
                    getCanvas().setFireBaseRef(mFireBaseRef.child(StaticData.BOARDSEGMENTS).child(key));
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // No-op
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        // Clean up our listener so we don't have it attached twice.
        mFireBaseRef.getRoot().child(".info/connected").removeEventListener(mConnectionListener);

        if (canvasView != null) {
            getCanvas().cleanup();
        }
        this.updateThumbnail(mBoardWidth, mBoardHeight, mSegmentsRef, mMetadataRef);
    }

    public CanvasView getCanvas() {
        return this.canvasView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_undu:
                getCanvas().undo();
                break;
            case R.id.ll_redu:
                getCanvas().redo();
                break;
            case R.id.ll_clear:
                getCanvas().clear();
                break;
            case R.id.btnMore:
                onMoreClick();
                break;
            default:
                break;
        }

    }

    public void onMoreClick() {
        DoodleEditContextMenuManager.getInstance().toggleContextMenuFromView(findViewById(R.id.btnMore), 0, this);
    }

    @Override
    public void onLineColorClick() {
        DoodleEditContextMenuManager.getInstance().hideContextMenu();
        PopUtils.showColourPicker(this, Color.BLACK, new PopUtils.IColourListner() {
            @Override
            public void onAccept(int colour) {

                getCanvas().setColor(colour);
            }

            @Override
            public void onCancel() {

            }
        });
    }

    @Override
    public void onLineThicknessClick() {
        DoodleEditContextMenuManager.getInstance().hideContextMenu();

        PopUtils.showSeekDialouge(this, selectedThicknessVallue, new PopUtils.ILineThickness() {
            @Override
            public void onSuccess(int value) {
                selectedThicknessVallue = value;
                getCanvas().setPaintThikness(value);
                Toast.makeText(DoodleEditActivity.this, "" + value, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {

            }
        });
    }


    @Override
    public void onCancelClick() {
        DoodleEditContextMenuManager.getInstance().hideContextMenu();
    }


    public static void updateThumbnail(int boardWidth, int boardHeight, Firebase segmentsRef, final Firebase metadataRef) {
        final float scale = Math.min(1.0f * THUMBNAIL_SIZE / boardWidth, 1.0f * THUMBNAIL_SIZE / boardHeight);
        final Bitmap b = Bitmap.createBitmap(Math.round(boardWidth * scale), Math.round(boardHeight * scale), Bitmap.Config.ARGB_8888);

        final Bitmap finalBitmap = StaticUtils.overlay(backgrounImageBitmap, b);
        final Canvas buffer = new Canvas(finalBitmap);

        buffer.drawRect(0, 0, finalBitmap.getWidth(), finalBitmap.getHeight(), CanvasView.paintFromColor(Color.TRANSPARENT, 1,Paint.Style.FILL_AND_STROKE));
        Log.i("DoodleEditActvity", "Generating thumbnail of " + finalBitmap.getWidth() + "x" + finalBitmap.getHeight());

        segmentsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot segmentSnapshot : dataSnapshot.getChildren()) {
                    Segment segment = segmentSnapshot.getValue(Segment.class);
                    buffer.drawPath(
                            CanvasView.getPathForPoints(segment.getPoints(), scale),
                            CanvasView.paintFromColor(segment.getColor(),segment.getThickNess())
                    );
                }
                String encoded = encodeToBase64(finalBitmap);
                metadataRef.child("thumbnail").setValue(encoded, new Firebase.CompletionListener() {
                    @Override
                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                        if (firebaseError != null) {
                            Log.e("DoodleEditActivity", "Error updating thumbnail", firebaseError.toException());
                        }
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public static String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = com.firebase.client.utilities.Base64.encodeBytes(b);

        if(image !=null)
            image.recycle();
        return imageEncoded;
    }

    public static Bitmap decodeFromBase64(String input) throws IOException {
        byte[] decodedByte = com.firebase.client.utilities.Base64.decode(input);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }


}