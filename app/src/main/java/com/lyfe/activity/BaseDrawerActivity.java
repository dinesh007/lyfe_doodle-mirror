package com.lyfe.activity;

import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lyfe.R;

/**
 * Created by Miroslaw Stanek on 15.07.15.
 */
public class BaseDrawerActivity extends BaseActivity {

    DrawerLayout drawerLayout;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentViewWithoutInject(R.layout.activity_drawer);
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.flContentRoot);
        LayoutInflater.from(this).inflate(layoutResID, viewGroup, true);
        initComponents();
        setMenuBtnAction();
    }

    private void initComponents() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
    }

    private void setMenuBtnAction() {
        if (getMenuButton() != null) {
            getMenuButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            });
        }
    }

    @Override
    protected void setupToolbar() {
        super.setupToolbar();
    }


//    private void setupHeader() {
//        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.global_menu_avatar_size);
//        this.profilePhoto = getResources().getString(R.string.user_profile_photo);
//        Picasso.with(this)
//                .load(profilePhoto)
//                .placeholder(R.drawable.img_circle_placeholder)
//                .resize(avatarSize, avatarSize)
//                .centerCrop()
//                .transform(new CircleTransformation())
//                .into(ivMenuUserProfilePhoto);
//    }
}
