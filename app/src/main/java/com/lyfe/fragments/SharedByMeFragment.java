package com.lyfe.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lyfe.R;
import com.lyfe.activity.DoodleEditActivity;
import com.lyfe.activity.DoodleViewActivity;
import com.lyfe.activity.MainActivity;
import com.lyfe.adapter.SharedDoodleInfoAdapter;
import com.lyfe.models.NewDoodleModel;
import com.lyfe.utils.PopUtils;
import com.lyfe.utils.view.DoodleContextMenu;
import com.lyfe.utils.view.DoodleContextMenuManager;

import java.util.HashMap;

public class SharedByMeFragment extends Fragment implements SharedDoodleInfoAdapter.OnFeedItemClickListener,
        DoodleContextMenu.OnDoodleContextMenuItemClickListener, View.OnClickListener {

    private View rootView;
    private ListView listView;
    private SharedDoodleInfoAdapter sharedDoodleInfoAdapter;

    public SharedByMeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shared_with_me, container, false);
        initComponents();
        setOnClickListeners();
        setDoodleList();
        return rootView;
    }

    private void setOnClickListeners() {
        ((MainActivity) getActivity()).fabCreate.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set up a notification to let us know when we're connected or disconnected from the Firebase servers
    }

    private void initComponents() {

        listView = (ListView) rootView.findViewById(R.id.rvFeed);
    }

    private void setDoodleList() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
//            @Override
//            protected int getExtraLayoutSpace(RecyclerView.State state) {
//                return 300;
//            }
//        };
//        listView.setLayoutManager(linearLayoutManager);
        sharedDoodleInfoAdapter = new SharedDoodleInfoAdapter<HashMap>(getActivity(), ((MainActivity) getActivity()).mBoardsRef, HashMap.class, true);
        sharedDoodleInfoAdapter.setOnFeedItemClickListener(this);
        listView.setAdapter(sharedDoodleInfoAdapter);
//        listView.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                DoodleContextMenuManager.getInstance().onScrolled(recyclerView, dx, dy);
//            }
//        });
    }


    @Override
    public void onCommentsClick(View v, int position) {

    }

    @Override
    public void onMoreClick(View v, int itemPosition) {
        DoodleContextMenuManager.getInstance().toggleContextMenuFromView(v, itemPosition, this);
    }

    @Override
    public void onDoodleSingleClicked(int pos) {
        Intent intent = new Intent(getActivity(), DoodleViewActivity.class);
        Bundle bundle = new Bundle();
        intent.putExtra("doodle_key", sharedDoodleInfoAdapter.getModelKey(pos));
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void onDoodleDoubleClicked(int pos) {
        Intent intent = new Intent(getActivity(), DoodleEditActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("doodle_key", sharedDoodleInfoAdapter.getModelKey(pos));
        bundle.putBoolean("is_create", false);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void onReportClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onBlockCreatorClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onUnFollowClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onCancelClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCreate:
                showCreateDoodleDialog();
                break;
            default:
                break;
        }
    }

    private void showCreateDoodleDialog() {
        PopUtils.showNewDoodler(getActivity(), new PopUtils.newDoodleListener() {
            @Override
            public void onSuccess(NewDoodleModel data) {
                navigateToNewDoodleEditActivity(data);
            }

            @Override
            public void onCancel() {

            }
        });
    }

    private void navigateToNewDoodleEditActivity(NewDoodleModel data) {
        Intent intent = new Intent(getActivity(), DoodleEditActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("doodle_data", data);
        bundle.putBoolean("is_create", true);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }
}
