package com.lyfe.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lyfe.R;
import com.lyfe.activity.DoodleEditActivity;
import com.lyfe.activity.DoodleViewActivity;
import com.lyfe.activity.MainActivity;
import com.lyfe.adapter.SharedDoodleInfoAdapter;
import com.lyfe.utils.view.DoodleContextMenu;
import com.lyfe.utils.view.DoodleContextMenuManager;

import java.util.HashMap;

/**
 * Created by SURESH on 10/28/2015.
 */
public class SharedWithMeFragment extends Fragment implements SharedDoodleInfoAdapter.OnFeedItemClickListener,
        DoodleContextMenu.OnDoodleContextMenuItemClickListener {

    private View rootView;
    private ListView listView;
    private SharedDoodleInfoAdapter sharedDoodleInfoAdapter;

    public SharedWithMeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shared_with_me, container, false);
        initComponents();
        setupFeed();
        return rootView;
    }

    private void initComponents() {
        listView = (ListView) rootView.findViewById(R.id.rvFeed);
    }

    private void setupFeed() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
//            @Override
//            protected int getExtraLayoutSpace(RecyclerView.State state) {
//                return 300;
//            }
//        };
//        listView.setLayoutManager(linearLayoutManager);
        sharedDoodleInfoAdapter = new SharedDoodleInfoAdapter<HashMap>(getActivity(), ((MainActivity) getActivity()).mBoardsRef, HashMap.class, true);
        sharedDoodleInfoAdapter.setOnFeedItemClickListener(this);
        listView.setAdapter(sharedDoodleInfoAdapter);
//        listView.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                DoodleContextMenuManager.getInstance().onScrolled(recyclerView, dx, dy);
//            }
//        });
    }


    @Override
    public void onCommentsClick(View v, int position) {
    }

    @Override
    public void onMoreClick(View v, int itemPosition) {
        DoodleContextMenuManager.getInstance().toggleContextMenuFromView(v, itemPosition, this);
    }

    @Override
    public void onDoodleSingleClicked(int position) {
        Intent intent = new Intent(getActivity(), DoodleViewActivity.class);
        intent.putExtra("doodle_key", sharedDoodleInfoAdapter.getModelKey(position));
        getActivity().startActivity(intent);
    }

    @Override
    public void onDoodleDoubleClicked(int position) {
        Intent intent = new Intent(getActivity(), DoodleEditActivity.class);
        intent.putExtra("is_create", false);
        intent.putExtra("doodle_key", sharedDoodleInfoAdapter.getModelKey(position));
        getActivity().startActivity(intent);
    }

    @Override
    public void onReportClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onBlockCreatorClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onUnFollowClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onCancelClick(int feedItem) {
        DoodleContextMenuManager.getInstance().hideContextMenu();
    }
}
