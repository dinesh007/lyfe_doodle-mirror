package com.lyfe.models;

import java.io.Serializable;

/**
 * Created by SriRam on 27-Oct-15.
 */
public class NewDoodleModel implements Serializable {

    private String name;
    private int size;
    private String noOfDoodles;
    private String timeLimite;
    private String visibility;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getNoOfDoodles() {
        return noOfDoodles;
    }

    public void setNoOfDoodles(String noOfDoodles) {
        this.noOfDoodles = noOfDoodles;
    }

    public String getTimeLimite() {
        return timeLimite;
    }

    public void setTimeLimite(String timeLimite) {
        this.timeLimite = timeLimite;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @Override
    public String toString() {
        return "NewDoodleModel{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", noOfDoodles=" + noOfDoodles +
                ", timeLimite=" + timeLimite +
                ", visibility='" + visibility + '\'' +
                '}';
    }
}
