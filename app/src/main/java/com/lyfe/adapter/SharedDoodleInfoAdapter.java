package com.lyfe.adapter;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.lyfe.R;
import com.lyfe.activity.DoodleEditActivity;
import com.lyfe.utils.StaticData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SharedDoodleInfoAdapter<T> extends BaseAdapter implements View.OnClickListener {

    private Context context;
    private boolean isSharedByMe;
    private OnFeedItemClickListener onFeedItemClickListener;
    private GestureDetector gestureDetector;

    private Query mRef;
    private Class<T> mModelClass;
    private List<T> mModels;
    private Map<String, T> mModelKeys;
    private ChildEventListener mListener;

    public SharedDoodleInfoAdapter(Context context, Query mRef, Class<T> mModelClass, boolean isSharedByMe) {
        this.context = context;
        this.mRef = mRef;
        this.mModelClass = mModelClass;
        mModels = new ArrayList<T>();
        mModelKeys = new HashMap<String, T>();
        this.isSharedByMe = isSharedByMe;
        gestureDetector = new GestureDetector(context, new GestureListener());
        setListener();
    }

    public void setOnFeedItemClickListener(OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }

    private void setListener() {
        mListener = this.mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {

                T model = dataSnapshot.getValue(mModelClass);
                mModelKeys.put(dataSnapshot.getKey(), model);

                // Insert into the correct location, based on previousChildName
                if (previousChildName == null) {
                    mModels.add(0, model);
                } else {
                    T previousModel = mModelKeys.get(previousChildName);
                    int previousIndex = mModels.indexOf(previousModel);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {
                        mModels.add(model);
                    } else {
                        mModels.add(nextIndex, model);
                    }
                }
                notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                // One of the mModels changed. Replace it in our list and name mapping
                String modelName = dataSnapshot.getKey();
                T oldModel = mModelKeys.get(modelName);
                T newModel = dataSnapshot.getValue(SharedDoodleInfoAdapter.this.mModelClass);
                int index = mModels.indexOf(oldModel);

                mModels.set(index, newModel);
                mModelKeys.put(modelName, newModel);

                notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                // A model was removed from the list. Remove it from our list and the name mapping
                String modelName = dataSnapshot.getKey();
                T oldModel = mModelKeys.get(modelName);
                mModels.remove(oldModel);
                mModelKeys.remove(modelName);
                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {

                // A model changed position in the list. Update our list accordingly
                String modelName = dataSnapshot.getKey();
                T oldModel = mModelKeys.get(modelName);
                T newModel = dataSnapshot.getValue(SharedDoodleInfoAdapter.this.mModelClass);
                int index = mModels.indexOf(oldModel);
                mModels.remove(index);
                if (previousChildName == null) {
                    mModels.add(0, newModel);
                } else {
                    T previousModel = mModelKeys.get(previousChildName);
                    int previousIndex = mModels.indexOf(previousModel);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {
                        mModels.add(newModel);
                    } else {
                        mModels.add(nextIndex, newModel);
                    }
                }
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
            }
        });
    }


    /**
     * Returns the key of the model, so that you can build a Firebase reference to it again.
     *
     * @param model the model to find the key for
     * @return the key of the model
     * @throws java.lang.IllegalArgumentException if the model is not present in the list
     */
    public String getModelKey(T model) {
        for (Map.Entry<String, T> entry : mModelKeys.entrySet()) {
            if (entry.getValue() == model) {
                return entry.getKey();
            }
        }
        throw new IllegalArgumentException("model not found");
    }

    /**
     * Returns the key of the model, so that you can build a Firebase reference to it again.
     *
     * @param model the model to find the key for
     * @return the key of the model
     * @throws java.lang.IllegalArgumentException if the model is not present in the list
     */
    public String getCreatorName(T model) {
        for (Map.Entry<String, T> entry : mModelKeys.entrySet()) {
            if (entry.getValue() == model) {
                return ((HashMap) entry.getValue()).get(StaticData.CREATED_BY).toString();
            }
        }
        throw new IllegalArgumentException("model not found");
    }

    /**
     * Returns the key of the model, so that you can build a Firebase reference to it again.
     *
     * @param i the index of the model to find the key for
     * @return the key of the model at index i
     */
    public String getModelKey(int i) {
        Object model = mModels.get(i);
        for (Map.Entry<String, T> entry : mModelKeys.entrySet()) {
            if (entry.getValue() == model) {
                return entry.getKey();
            }
        }
        throw new IllegalArgumentException("model not found");
    }


    private int imagePosition = -1;

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ViewHolder {
        private ImageView ivFeedCenter;
        private TextView txtSharedContent, txtCreatedByName, txtCreatedForName;
        private ImageButton btnLike, btnDownload, btnShare, btnComments, btnMore;
        private FrameLayout vImageRoot;
        private LinearLayout vCreatedByRoot, vCreatedForRoot;
        private ImageView ivCreatedByUserProfile, ivCreatedForUserProfile;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_shared_doodle_item, null, false);
            viewHolder = new ViewHolder();
            viewHolder.ivFeedCenter = (ImageView) convertView.findViewById(R.id.ivFeedCenter);
            viewHolder.txtSharedContent = (TextView) convertView.findViewById(R.id.txt_shared_content);
            viewHolder.btnLike = (ImageButton) convertView.findViewById(R.id.btnLike);
            viewHolder.btnDownload = (ImageButton) convertView.findViewById(R.id.btnDownload);
            viewHolder.btnShare = (ImageButton) convertView.findViewById(R.id.btnShare);
            viewHolder.btnComments = (ImageButton) convertView.findViewById(R.id.btnComments);
            viewHolder.btnMore = (ImageButton) convertView.findViewById(R.id.btnMore);
            viewHolder.vImageRoot = (FrameLayout) convertView.findViewById(R.id.vImageRoot);
            viewHolder.vCreatedByRoot = (LinearLayout) convertView.findViewById(R.id.shared_doodle_ll_created_by);
            viewHolder.vCreatedForRoot = (LinearLayout) convertView.findViewById(R.id.shared_doodle_ll_created_for);
            viewHolder.ivCreatedByUserProfile = (ImageView) convertView.findViewById(R.id.shared_doodle_iv_created_by);
            viewHolder.ivCreatedForUserProfile = (ImageView) convertView.findViewById(R.id.shared_doodle_iv_created_for);
            viewHolder.txtCreatedByName = (TextView) convertView.findViewById(R.id.shared_doodle_txt_created_by_name);
            viewHolder.txtCreatedForName = (TextView) convertView.findViewById(R.id.shared_doodle_txt_created_for_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.btnComments.setOnClickListener(this);
        viewHolder.btnMore.setOnClickListener(this);
        viewHolder.ivFeedCenter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    imagePosition = (int) v.getTag();
                    gestureDetector.onTouchEvent(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
        viewHolder.btnLike.setOnClickListener(this);
        HashMap model = (HashMap) mModels.get(position);
        if (model.get("thumbnail") != null) {
            try {
                viewHolder.ivFeedCenter.setImageBitmap(DoodleEditActivity.decodeFromBase64(model.get("thumbnail").toString()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            viewHolder.ivFeedCenter.setVisibility(View.INVISIBLE);
        }
        if (isSharedByMe) {
            viewHolder.vCreatedByRoot.setVisibility(View.GONE);
            viewHolder.vCreatedForRoot.setVisibility(View.VISIBLE);
            viewHolder.txtCreatedForName.setText(((HashMap) mModels.get(position)).get(StaticData.CREATED_BY).toString());
        } else {
            viewHolder.vCreatedByRoot.setVisibility(View.VISIBLE);
            viewHolder.vCreatedForRoot.setVisibility(View.GONE);
            viewHolder.txtCreatedByName.setText(((HashMap) mModels.get(position)).get(StaticData.CREATED_BY).toString());
        }
        viewHolder.btnComments.setTag(position);
        viewHolder.btnMore.setTag(position);
        viewHolder.ivFeedCenter.setTag(position);
        viewHolder.btnLike.setTag(position);

        return convertView;
    }


    @Override
    public void onClick(View view) {
        final int viewId = view.getId();
        if (viewId == R.id.btnComments) {
            if (onFeedItemClickListener != null) {
                onFeedItemClickListener.onCommentsClick(view, (Integer) view.getTag());
            }
        } else if (viewId == R.id.btnMore) {
            if (onFeedItemClickListener != null) {
                onFeedItemClickListener.onMoreClick(view, (Integer) view.getTag());
            }
        } else if (viewId == R.id.btnLike) {
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private int pos;

        private void setSelectedPosition(int pos) {
            this.pos = pos;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (onFeedItemClickListener != null) {
                onFeedItemClickListener.onDoodleSingleClicked(imagePosition);
            }
            return super.onSingleTapConfirmed(e);
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (onFeedItemClickListener != null) {
                onFeedItemClickListener.onDoodleDoubleClicked(imagePosition);
            }
            return true;
        }
    }

    public interface OnFeedItemClickListener {
        public void onCommentsClick(View v, int position);

        public void onMoreClick(View v, int position);

        public void onDoodleSingleClicked(int pos);

        public void onDoodleDoubleClicked(int pos);
    }
}
