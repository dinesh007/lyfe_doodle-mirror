package com.lyfe;

import android.app.Application;

import com.android.volley.toolbox.MyVolley;
import com.firebase.client.Firebase;
import com.lyfe.utils.StaticData;
import com.lyfe.utils.SyncedBoardManager;

/**
 * Created by froger_mcs on 05.11.14.
 */
public class LyfeApplication extends Application {

    private static LyfeApplication instance;
    private Firebase fireBaseRef;

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
        MyVolley.init(getApplicationContext());
        //Firebase.getDefaultConfig().setLogLevel(Logger.Level.DEBUG);
        SyncedBoardManager.setContext(this);
    }

    public static synchronized LyfeApplication getInstance() {
        if (instance == null) {
            instance = new LyfeApplication();
        }
        return instance;
    }

    public Firebase getFireBaseReference() {
        if (fireBaseRef == null) {
            fireBaseRef = new Firebase(StaticData.FIRE_BASE_URL);
        }
        return fireBaseRef;
    }

}
