package com.lyfe.utils.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lyfe.R;
import com.lyfe.utils.StaticUtils;

/**
 * Created by froger_mcs on 15.12.14.
 */
public class DoodleEditContextMenu extends LinearLayout implements View.OnClickListener {
    private static final int CONTEXT_MENU_WIDTH = StaticUtils.dpToPx(240);

    private OnDoodleEditContextMenuItemClickListener onItemClickListener;
    private View rootView;

    public DoodleEditContextMenu(Context context) {
        super(context);
        init();
    }

    public void bindToItem(int feedItem) {
    }

    private void init() {
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.doodle_edit_context_menu, this, true);
        setBackgroundResource(R.drawable.bg_container_shadow);
        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(CONTEXT_MENU_WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT));
        setClickListeners();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private void setClickListeners() {
        rootView.findViewById(R.id.btnLineColor).setOnClickListener(this);
        rootView.findViewById(R.id.btnLineThickness).setOnClickListener(this);
        rootView.findViewById(R.id.btnCancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLineColor:
                onLineColorClick();
                break;
            case R.id.btnLineThickness:
                onLineThicknessClick();
                break;
            case R.id.btnCancel:
                onCancelClick();
                break;
        }
    }


    public void dismiss() {
        ((ViewGroup) getParent()).removeView(DoodleEditContextMenu.this);
    }

    public void onLineColorClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onLineColorClick();
        }
    }

    public void onLineThicknessClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onLineThicknessClick();
        }
    }

    public void onCancelClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onCancelClick();
        }
    }

    public void setOnMenuItemClickListener(OnDoodleEditContextMenuItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnDoodleEditContextMenuItemClickListener {
        public void onLineColorClick();

        public void onLineThicknessClick();

        public void onCancelClick();
    }
}