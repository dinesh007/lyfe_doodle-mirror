package com.lyfe.utils.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lyfe.R;
import com.lyfe.utils.StaticUtils;

/**
 * Created by froger_mcs on 15.12.14.
 */
public class DoodleContextMenu extends LinearLayout implements View.OnClickListener {
    private static final int CONTEXT_MENU_WIDTH = StaticUtils.dpToPx(240);

    private int feedItem = -1;
    private View rootView;


    private OnDoodleContextMenuItemClickListener onItemClickListener;

    public DoodleContextMenu(Context context) {
        super(context);
        init();
    }

    private void init() {
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.shared_doodle_context_menu, this, true);
        setBackgroundResource(R.drawable.bg_container_shadow);
        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(CONTEXT_MENU_WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT));
        setClickListeners();
    }

    private void setClickListeners() {
        rootView.findViewById(R.id.btnReport).setOnClickListener(this);
        rootView.findViewById(R.id.btnBlockCreater).setOnClickListener(this);
        rootView.findViewById(R.id.btnUnFollow).setOnClickListener(this);
        rootView.findViewById(R.id.btnCancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReport:
                onReportClick();
                break;
            case R.id.btnBlockCreater:
                onBlockCreatorClick();
                break;
            case R.id.btnUnFollow:
                onUnFollowClick();
                break;
            case R.id.btnCancel:
                onCancelClick();
                break;
        }
    }

    public void bindToItem(int feedItem) {
        this.feedItem = feedItem;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public void dismiss() {
        ((ViewGroup) getParent()).removeView(DoodleContextMenu.this);
    }

    public void onReportClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onReportClick(feedItem);
        }
    }

    public void onBlockCreatorClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onBlockCreatorClick(feedItem);
        }
    }

    public void onUnFollowClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onUnFollowClick(feedItem);
        }
    }

    public void onCancelClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onCancelClick(feedItem);
        }
    }

    public void setOnMenuItemClickListener(OnDoodleContextMenuItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public interface OnDoodleContextMenuItemClickListener {
        public void onReportClick(int feedItem);

        public void onBlockCreatorClick(int feedItem);

        public void onUnFollowClick(int feedItem);

        public void onCancelClick(int feedItem);
    }
}