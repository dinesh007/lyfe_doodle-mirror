package com.lyfe.utils;

/**
 * Created by hb on 5/11/15.
 */
public class StaticData {

    //    public static String FIRE_BASE_URL = "https://doodleboard.firebaseio.com/";
    public static String FIRE_BASE_URL = "https://lyfedoodleapp.firebaseio.com/";

    //    public static String FIRE_BASE_URL  = "https://lyfe-doodlw.firebaseio.com/";
    public static String BOARDMETAS = "boardmetas";
    public static String BOARDSEGMENTS = "boardsegments";

    public static String CREATED_AT = "created_at";
    public static String CREATED_BY = "created_by";
    public static String WIDTH = "width";
    public static String HEIGHT = "height";
}
