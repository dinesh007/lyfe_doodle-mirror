package com.hb.avaible.wsutils;

import android.os.Bundle;
import android.util.Log;

import java.net.URLEncoder;

/**
 * Created by hb on 4/9/15.
 */
public class WSUtils {

    public static final String TAG = "AvaibleApp";
    public static final int CONNECTION_TIMEOUT = 40 * 1000;


    /**
     * @WS request codes
     */
    public static final int REQ_FACEBOOK_IMAGE = 100;
    public static final int REQ_PLACES_AUTOCOMPLETE = 101;
    public static final int REQ_SIGN_UP = 102;
    public static final int REQ_LOGIN = 103;
    public static final int REQ_EDIT_PROFILE = 105;
    public static final int REQ_FORGET_PASSWORD = 104;
    public static final int REQ_DELETE_FRIEND = 106;
    public static final int REQ_ACCEPT_FRIEND_REQUEST = 107;
    public static final int REQ_DECLINE_FRIEND_REQUEST = 108;
    public static final int REQ_CONTACT_LIST = 109;
    public static final int REQ_SETTING = 110;
    public static final int REQ_MY_PROFILE = 111;
    public static final int REQ_LEAVE_GROUP = 112;
    public static final int REQ_GROUP_LIST = 113;
    public static final int REQ_CREATE_POST = 114;
    public static final int REQ_DELETE_POST = 115;
    public static final int REQ_JOIN_POST = 116;
    public static final int REQ_POST_LISTS = 117;
    public static final int REQ_EDIT_POST = 118;
    public static final int REQ_PENDING_REQUEST_LIST = 119;
    public static final int REQ_PENDING_GROUP_REQUEST_LIST = 120;
    public static final int REQ_ACCEPT_GROUP_REQUEST = 121;
    public static final int REQ_DECLINE_GROUP_REQUEST = 122;
    public static final int REQ_VIEW_PROFILE = 123;
    public static final int REQ_FRIEND_REQUEST = 124;
    public static final int REQ_ADD_GROUP = 125;
    public static final int REQ_UPDATE_GROUP = 126;
    public static final int REQ_GROUP_DETAIL = 127;
    public static final int REQ_DELETE_GROUP = 128;
    public static final int REQ_UPCOMING_EVENT = 129;
    public static final int REQ_POST_MEMBER = 130;
    public static final int REQ_FRIENDS_LIST = 131;
    public static final int REQ_GROUP_FRIEND_LIST = 132;
    public static final int REQ_UPLOAD_POST_IMAGE = 133;
    public static final int REQ_SET_ARN = 134;
    public static final int REQ_FACEBOOK_LOGIN = 135;
    public static final int REQ_CHANGE_PASSWORD = 136;
    public static final int REQ_SEND_OFFLINE_PUSHNOTIFICATION = 137;
    public static final int REQ_REMOVE_POST_USER = 138;

    public static final String CHAT_SERVER = "66.85.130.226";
    public static final int CHAT_SERVER_PORT = 5222;
    public static final String CHAT_DOMAIN = "@staging.projectspreview.com";
    public static final String CHAT_GROUP_DOMAIN = "@conference.staging.projectspreview.com";
    public static final String XMPP_PASSWORD = "avaible123";
    public static String KEY_RESPONSE_STATUS = "response_status";
    public static String KEY_MESSAGE = "message";
    public static String KEY_DATA = "data";
    public static String KEY_URL = "url";
    public static String KEY_ERROR = "error";

    /**
     * @WS Keys
     */
    public static String KEY_CLASS = "class";
    public static String KEY_VPHONENUMBER = "vPhonenumber";
    public static String KEY_VEMAIL = "vEmail";
    public static String KEY_VFULLNAME = "vFullName";
    public static String KEY_VCOUNTRYCODE = "vCountryCode";
    public static String KEY_VPROFILEIMAGE = "vProfileImage";
    public static String KEY_VTOKEN = "vToken";
    public static String KEY_IUSERID = "iUserId";
    public static String KEY_XMPPID = "vXmppId";
    public static String KEY_TIMESTAMP = "timestamp";

    public static String KEY_IGROUPID = "iGroupId";
    public static String KEY_VGROUPNAME = "vGroupName";
    public static String KEY_GROUP_DETAILS = "group_details";
    public static String KEY_GROUP_MEMBERS = "group_members";
    public static String KEY_VGROUPPROFILEIMAGE = "vGroupProfileImage";
    public static String KEY_ECUSTOMIMAGESTATUS = "eCustomImageStatus";
    public static String KEY_ICUSTOMIMAGEID = "iCustomImageId";
    public static String KEY_ESTATUS = "eStatus";
    public static String KEY_DINSERTEDDATETIME = "dInsertedDateTime";
    public static String KEY_DUPDATEDDATETIME = "dUpdatedDateTime";
    public static String KEY_ISYSRECDELETED = "iSysRecDeleted";
    public static String KEY_GROUP_MEMBER_COUNT = "group_member_count";
    public static String KEY_GROUP_POST_STATUS = "group_post_status";

    public static String KEY_EMAKESPOSTNOTIFICATION = "eMakesPostNotification";
    public static String KEY_EJOINSMYPOSTNOTIFICATION = "eJoinsMyPostNotification";
    public static String KEY_EWRITESAMESSAGENOTIFICATION = "eWritesAMessageNotification";
    public static String KEY_ESENDSFRIENDREQUESTNOTIFICATION = "eSendsFriendRequestNotification";
    public static String KEY_EACCEPTMYFRIENDREQUESTNOTIFICATION = "eAcceptMyFriendRequestNotification";
    public static String KEY_ESAVEIMAGESINTOCAMERAROLE = "eSaveImagesIntoCameraRole";
    public static String KEY_EPOSTSHAREFUNCTION = "ePostShareFunction";
    public static String KEY_FRIENDS = "friends";
    public static String KEY_EMAIL_MESSAGE = "email_message";
    public static String KEY_FRIEND = "friend";
    public static String KEY_IPOSTID = "iPostId";
    public static String KEY_VPOSTTITLE = "vPostTitle";
    public static String KEY_VLATITUDE = "vLatitude";
    public static String KEY_VLONGITUDE = "vLongitude";
    public static String KEY_VLOCATION = "vLocation";
    public static String KEY_VXMPPPASSWORD = "vXmppPassword";
    public static String KEY_DPOSTDATE = "dPostDate";
    public static String KEY_DPOSTCREATEDDATE = "postCreatedDate";
    public static String KEY_ICREATEDBY = "iCreatedBy";
    public static String KEY_ETYPE = "eType";
    public static String KEY_CREATEDBY_FIRSTNAME = "createdby_firstname";
    public static String KEY_CREATEDBY_LASTNAME = "createdby_lastname";
    public static String KEY_CREATEDBY_FULLNAME = "createdby_fullname";
    public static String KEY_CREATEDBY_PROFILEIMAGE = "createdby_profileimage";
    public static String KEY_POST_MEMBER_COUNT = "post_member_count";
    public static String KEY_IFRIENDINVITEID = "iFriendInviteId";
    public static String KEY_IINVITERID = "iInviterId";
    public static String KEY_VFIRSTNAME = "vFirstName";
    public static String KEY_VLASTNAME = "vLastName";
    public static String KEY_REQUESTEDTIME = "requestedTime";
    public static String KEY_PEOPLEINVITED = "peopleInvited";
    public static String KEY_POSTS = "posts";
    public static String KEY_POST_XMPPID = "vXmppId";
    public static String KEY_MUTUAL_FRIEND = "mutual_friend";
    public static String KEY_IFRIENDID = "iFriendId";
    public static String KEY_IFRIENDUSERID = "iFriendUserId";
    public static String KEY_FRIEND_STATUS = "friend_status";
    public static String KEY_FRIEND_POST_STATUS = "friend_post_status";
    //  upcoming events key
    public static String KEY_UPCOMINGPOST = "users_upcoming_posts";
    public static String KEY_COMMONPOST = "users_common_posts";
    public static String KEY_FRIEND_REQUESTS = "friend_requests";
    public static String KEY_FRIEND_REQUEST = "friend_request";
    public static String KEY_FRIENDREQUEST = "friendRequest";
    public static String KEY_GROUP_REQUESTS = "group_requests";
    public static String KEY_GROUPCREATER = "groupCreater";
    public static String KEY_GROUPMEMBERS = "groupMembers";
    public static String KEY_IGROUPINVITEID = "iGroupInviteId";
    public static String KEY_VSENDERID = "vSenderId";
    public static String KEY_IMAGE = "vImage";
    public static String KEY_VFACEBOOKID = "vFacebookId";
    public static String KEY_IPOSTMEMBERID = "iPostMemberId";
    public static String KEY_MUTUAL_FRIENDS = "mutual_friends";
    public static String KEY_OTHER_FRIENDS = "other_friends";


    public static final String LOCAL_URL1 = "http://192.168.36.14/Projects/Core/available/";
    public static final String LOCAL_URL2 = "http://192.168.36.14/Projects/Core/AVAIBLE-Api/";
    public static final String LOCAL_LIVE = "http://108.170.62.152/webservice/ws_avaible/";
    public static final String STAGING_URL = "http://202.131.125.130:8000/AVAIBLE/";

    /**
     * @ws urls
     */
    public static final String BASE_URL = STAGING_URL;
    public static final String WS_PLACES_AUTOCOMPLETE = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    public static final String WS_SIGN_UP = BASE_URL + "sign_up.asmx?";
    public static final String WS_FORGET_PASSWORD = BASE_URL + "forgot_password.asmx?";
    public static final String WS_DELETE_FRIEND = BASE_URL + "delete_friend.asmx?";
    public static final String WS_LEAVE_GROUP = BASE_URL + "leave_group.asmx?";
    public static final String WS_GROUP_LIST = BASE_URL + "group_list.asmx?";

    public static final String WS_LOGIN = BASE_URL + "login.asmx?";
    public static final String WS_FACEBOOK_LOGIN = BASE_URL + "fb_sign_up.asmx?";
    public static final String WS_SET_ARN = BASE_URL + "set_arn.asmx?";
    public static final String WS_EDIT_PROFILE = BASE_URL + "edit_profile.asmx?";
    public static final String WS_ACCEPT_FRIEND_REQUEST = BASE_URL + "accept_friend_request.asmx?";
    public static final String WS_DECLINE_FRIEND_REQUEST = BASE_URL + "decline_friend_request.asmx?";
    public static final String WS_CONTACT_LIST = BASE_URL + "contact_list.asmx?";
    public static final String WS_SETTINGS = BASE_URL + "update_notification_status.asmx?";
    public static final String WS_MY_PROFILE = BASE_URL + "my_profile.asmx?";
    public static final String WS_CREATE_POST = BASE_URL + "create_post.asmx?";
    public static final String WS_DELETE_POST = BASE_URL + "delete_post.asmx?";
    public static final String WS_JOIN_POST = BASE_URL + "join_post.asmx?";
    public static final String WS_POST_LISTS = BASE_URL + "post_lists.asmx?";
    public static final String WS_EDIT_POST = BASE_URL + "edit_post.asmx?";
    public static final String WS_GROUP_DETAIL = BASE_URL + "group_details.asmx?";
    public static final String WS_PENDING_REQUEST_LIST = BASE_URL + "pending_request_list.asmx?";
    public static final String WS_PENDING_GROUP_REQUEST_LIST = BASE_URL + "pending_group_request_list.asmx?";
    public static final String WS_ACCEPT_GROUP_REQUEST = BASE_URL + "accept_group_request.asmx?";
    public static final String WS_DECLINE_GROUP_REQUEST = BASE_URL + "decline_group_request.asmx?";
    public static final String WS_VIEW_PROFILE = BASE_URL + "view_profile.asmx?";
    public static final String WS_FRIEND_REQUEST = BASE_URL + "friend_request.asmx?";
    public static final String WS_DELETE_GROUP = BASE_URL + "delete_group.asmx?";
    public static final String WS_UPCOMING_EVENT = BASE_URL + "upcoming_post_lists.asmx?";
    public static final String WS_ADD_GROUP = BASE_URL + "add_group.asmx?";
    public static final String WS_UPDATE_GROUP = BASE_URL + "update_group.asmx?";
    public static final String WS_FRIEND_LIST = BASE_URL + "friend_list.asmx?";
    public static final String WS_POST_MEMBER = BASE_URL + "post_members.asmx?";
    public static final String WS_GROUP_FRIEND_LIST = BASE_URL + "friend_group_list.asmx?";
    public static final String WS_UPLOAD_POST_IMAGE = BASE_URL + "upload_post_image.asmx?";
    public static final String WS_VIEW_USER_PROFILE_IMAGE = BASE_URL + "view_user_profile_image.php?";
    public static final String WS_CHANGE_PASSWORD = BASE_URL + "change_password.asmx?";
    public static final String WS_SEND_OFFLINE_PUSHNOTIFICATION = BASE_URL + "send_offline_pushnotification.asmx?";
    public static final String WS_REMOVE_POST_USER = BASE_URL + "remove_post_user.asmx?";

    @SuppressWarnings("deprecation")
    public static String encodeGETUrl(Bundle parameters) {
        StringBuilder sb = new StringBuilder();
        if (parameters != null && parameters.size() > 0) {
            boolean first = true;
            for (String key : parameters.keySet()) {
                if (key != null) {
                    if (first) {
                        first = false;
                    } else {
                        sb.append("&");
                    }
                    String value = "";
                    Object object = parameters.get(key);
                    if (object != null) {
                        value = String.valueOf(object);
                    }
                    try {
                        sb.append(URLEncoder.encode(key, "UTF-8") + "="
                                + URLEncoder.encode(value, "UTF-8"));
                    } catch (Exception e) {
                        sb.append(URLEncoder.encode(key) + "="
                                + URLEncoder.encode(value));
                    }
                }
            }
        }
        return sb.toString();
    }

    public static String encodeUrl(String url, Bundle mParams) {
        String URL;
        URL = url + encodeGETUrl(mParams);
        Log.e("Webserveice", URL);
        return URL;
    }
}
