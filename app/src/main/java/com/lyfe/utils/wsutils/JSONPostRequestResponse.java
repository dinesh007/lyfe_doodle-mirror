//package com.lyfe.utils.wsutils;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.MultipartRequest;
//import com.android.volley.toolbox.MyVolley;
//import com.android.volley.toolbox.StringRequest;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.util.HashMap;
//import java.util.Map;
//
//public class JSONPostRequestResponse {
//
//    private static final Object _tag_req = "Avaible";
//    Map<String, String> params1;
//    private int reqCode;
////    private IParseListener listner;
////    private IParseListenerV2 v2Listner;
//
//    private boolean isFile = false;
//    private String file_path = "", key = "";
//    private HashMap<String, File> mAttachFileList = new HashMap<String, File>();
//    private Context context;
//    private String messageId;
//
//    public JSONPostRequestResponse(Context context) {
//        this.context = context;
//    }
//
//
//    public void setRequest(String url, final int requestCode, IParseListenerV2 mParseListener, String messageId) {
//        setRequest(url, requestCode, mParseListener, messageId);
//    }
//
//    public void setRequest(String url, final int requestCode, IParseListenerV2 mParseListener, Bundle params, final ChatMessage chatMessage) {
//        this.v2Listner = mParseListener;
//        this.reqCode = requestCode;
//        this.messageId = messageId;
//
//        Response.Listener<JSONObject> sListener = new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                if (v2Listner != null) {
//                    v2Listner.SuccessResponse(response, reqCode, chatMessage);
//                }
//            }
//        };
//
//        Response.ErrorListener eListener = new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                if (v2Listner != null) {
//                    v2Listner.ErrorResponse(error, reqCode, chatMessage);
//                }
//            }
//        };
//        if (file_path != null) {
//            if (file_path != null) {
//                String[] mary = file_path.split(",");
//                if (mary.length > 1) {
//                    File mFile = new File(mary[0]);
//                    MultipartRequest multipartRequest = new MultipartRequest(
//                            url, eListener, sListener, key, mary, mFile,
//                            params, mAttachFileList);
//
//                    multipartRequest.setAttachFileList(mAttachFileList);
//                    MyVolley.getRequestQueue().add(multipartRequest);
//
//                } else {
//                    File mFile = new File(mary[0]);
//                    MultipartRequest multipartRequest = new MultipartRequest(
//                            url, eListener, sListener, key, mFile, params,
//                            mAttachFileList);
//                    multipartRequest.setAttachFileList(mAttachFileList);
//                    MyVolley.getRequestQueue().add(multipartRequest);
//                }
//            } else {
//                throw new NullPointerException("File path is null");
//            }
//        } else {
//            throw new NullPointerException("File path is null");
//        }
//
//    }
//
//    public void getResponseObject(String url, final int requestCode, IParseListener mParseListener) {
//        if (!StaticUtils.isNetworkAvailable(context)) {
//            if (mParseListener != null) {
//                try {
//                    mParseListener.NoInternetConnection(context.getString(R.string.check_internet_connection));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        } else {
//            getResponse(url, requestCode, WSUtils.TAG, mParseListener, null);
//        }
//    }
//
//    public void getResponse(String url, final int requestCode, String tag, IParseListener mParseListener, Bundle params) {
//        if (!StaticUtils.isNetworkAvailable(context)) {
//            if (mParseListener != null)
//                mParseListener.NoInternetConnection(context.getString(R.string.check_internet_connection));
//        } else {
//            this.listner = mParseListener;
//            this.reqCode = requestCode;
//
//            Response.Listener<JSONObject> sListener = new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    if (listner != null) {
//                        listner.SuccessResponse(response, reqCode);
//                    }
//                }
//            };
//            Response.ErrorListener eListener = new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    if (listner != null) {
//                        String Error = "";
//                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                            Error = "Request time out. Please check you Network connection and try again";
//                        } else if (error instanceof AuthFailureError) {
//                            Error = "Authentication Faliure";
//                        } else if (error instanceof ServerError) {
//                            Error = "Error contacting server";
//                        } else if (error instanceof NetworkError) {
//                            Error = "Network Error. Please check you Network connection and try again";
//                        } else if (error instanceof ParseError) {
//                            Error = "Network Error. Please check you Network connection and try again";
//                        } else {
//                            Error = "Something went wrong! Please try again later";
//                        }
//                        listner.ErrorResponse(error, reqCode);
//                    }
//                }
//            };
//
//
//            if (!isFile) {
//                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, sListener, eListener);
//                MyVolley.getRequestQueue().add(jsObjRequest);
//                if (tag != null)
//                    jsObjRequest.setTag(tag);
//                else
//                    jsObjRequest.setTag(WSUtils.TAG);
//            } else {
//                if (file_path != null) {
//                    if (file_path != null) {
//                        String[] mary = file_path.split(",");
//                        if (mary.length > 1) {
//                            File mFile = new File(mary[0]);
//                            MultipartRequest multipartRequest = new MultipartRequest(
//                                    url, eListener, sListener, key, mary, mFile,
//                                    params, mAttachFileList);
//
//                            multipartRequest.setAttachFileList(mAttachFileList);
//                            MyVolley.getRequestQueue().add(multipartRequest);
//
//                        } else {
//                            File mFile = new File(mary[0]);
//                            MultipartRequest multipartRequest = new MultipartRequest(
//                                    url, eListener, sListener, key, mFile, params,
//                                    mAttachFileList);
//                            multipartRequest.setAttachFileList(mAttachFileList);
//                            MyVolley.getRequestQueue().add(multipartRequest);
//                        }
//                    } else {
//                        throw new NullPointerException("File path is null");
//                    }
//                } else {
//                    throw new NullPointerException("File path is null");
//                }
//            }
//        }
//    }
//
//    /**
//     * @return the isFile
//     */
//    public boolean isFile() {
//        return isFile;
//    }
//
//    public void setFile(String param, String path) {
//        if (path != null && param != null) {
//            if (path.contains("null"))
//                path = path.replace("null", "");
//            key = param;
//            file_path = path;
//            this.isFile = true;
//        }
//    }
//
//    public HashMap<String, File> getAttachFileList() {
//        return mAttachFileList;
//    }
//
//    public void setAttachFileList(HashMap<String, File> mAttachFileList) {
//        this.isFile = true;
//        this.mAttachFileList = mAttachFileList;
//    }
//
//    public void postJSONData(String url, final int requestCode, final IParseListener mParseListener, Map<String, String> params) {
//        this.listner = mParseListener;
//        this.reqCode = requestCode;
//        this.params1 = params;
//        Log.e("Post WS params", url + "WS Params  =  " + params.toString());
//        if (StaticUtils.isNetworkAvailable(context)) {
//            StringRequest postRequest = new StringRequest(Request.Method.POST,
//                    url, new Response.Listener<String>() {
//                public void onResponse(String response) {
//                    Log.e("PostRequestResponse ", "Resoponse  = " + response);
//                    if (mParseListener != null) {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            mParseListener.SuccessResponse(jsonObject, reqCode);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }, new Response.ErrorListener() {
//                public void onErrorResponse(VolleyError error) {
//                    if (mParseListener != null) {
//                        String Error = "";
//                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                            Error = "Request time out. Please check you Network connection and try again";
//                        } else if (error instanceof AuthFailureError) {
//                            Error = "Authentication Faliure";
//                        } else if (error instanceof ServerError) {
//                            Error = "Error contacting server";
//                        } else if (error instanceof NetworkError) {
//                            Error = "Network Error. Please check you Network connection and try again";
//                        } else if (error instanceof ParseError) {
//                            Error = "Network Error. Please check you Network connection and try again";
//                        } else {
//                            Error = "Something went wrong! Please try again later";
//                        }
//                        listner.ErrorResponse(error, reqCode);
//                    }
//                }
//            }) {
//                protected Map<String, String> getParams() {
//                    return params1;
//                }
//            };
//            postRequest.setTag(_tag_req);
//            MyVolley.getRequestQueue().add(postRequest);
//        } else {
//            if (mParseListener != null) {
//                try {
//                    mParseListener.NoInternetConnection(context.getString(R.string.check_internet_connection));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    public void postJSONData(String url, final int requestCode, IParseListener mParseListener, Bundle params) {
//        this.listner = mParseListener;
//        this.reqCode = requestCode;
//        Log.e("Post WS params", url + "WS Params  =  " + params.toString());
//        if (StaticUtils.isNetworkAvailable(context)) {
//            Response.Listener<JSONObject> sListener = new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    if (listner != null) {
//                        listner.SuccessResponse(response, reqCode);
//                    }
//                }
//            };
//            Response.ErrorListener eListener = new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    if (listner != null) {
//                        String Error = "";
//                        if (error instanceof TimeoutError
//                                || error instanceof NoConnectionError) {
//                            Error = "Request time out. Please check you Network connection and try again";
//                        } else if (error instanceof AuthFailureError) {
//                            Error = "Authentication Faliure";
//                        } else if (error instanceof ServerError) {
//                            Error = "Error contacting server";
//                        } else if (error instanceof NetworkError) {
//                            Error = "Network Error. Please check you Network connection and try again";
//                        } else if (error instanceof ParseError) {
//                            Error = "Network Error. Please check you Network connection and try again";
//                        } else {
//                            Error = "Something went wrong! Please try again later";
//                        }
//                        Log.v("Error Message", Error);
//                        listner.ErrorResponse(error, reqCode);
//                    }
//                }
//            };
//            if (file_path != null) {
//                if (file_path != null) {
//                    String[] mary = file_path.split(",");
//                    if (mary.length > 1) {
//                        File mFile = new File(mary[0]);
//                        MultipartRequest multipartRequest = new MultipartRequest(
//                                url, eListener, sListener, key, mary, mFile,
//                                params, mAttachFileList);
//
//                        multipartRequest.setAttachFileList(mAttachFileList);
//                        MyVolley.getRequestQueue().add(multipartRequest);
//
//                    } else {
//                        File mFile = new File(mary[0]);
//                        MultipartRequest multipartRequest = new MultipartRequest(
//                                url, eListener, sListener, key, mFile, params,
//                                mAttachFileList);
//                        multipartRequest.setAttachFileList(mAttachFileList);
//                        MyVolley.getRequestQueue().add(multipartRequest);
//                    }
//                } else {
//                    throw new NullPointerException("File path is null");
//                }
//            } else {
//                throw new NullPointerException("File path is null");
//            }
//        } else {
//            if (mParseListener != null) {
//                Log.v("JSONRequest", "No Internet 1");
//                mParseListener.NoInternetConnection(context.getString(R.string.check_internet_connection));
//            }
//        }
//    }
//
//    public void cancelRequest(String tag) {
//        MyVolley.getRequestQueue().cancelAll(tag);
//    }
//}