package com.lyfe.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.support.annotation.ColorInt;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.larswerkman.lobsterpicker.LobsterPicker;
import com.larswerkman.lobsterpicker.OnColorListener;
import com.lyfe.R;
import com.lyfe.models.NewDoodleModel;

public class PopUtils {
    private static int size = 640;
    private static String numOfDoodlers = "3";
    private static String time = "30 Secs";
    private static String visibility = "Public";

    private static int seekValue = 1;

    public interface ILineThickness {
        void onSuccess(int value);

        void onCancel();
    }

    public interface newDoodleListener {
        void onSuccess(NewDoodleModel data);

        void onCancel();
    }

    public interface IColourListner {
        void onAccept(int colour);

        void onCancel();
    }


    public static void showSeekDialouge(Activity context, final int value, final ILineThickness ILineThickness) {
        try {
            AlertDialog.Builder alertDialogBuilder = getBuilder(context);
            final View v = LayoutInflater.from(context).inflate(R.layout.layout_seekbar, null, false);
            SeekBar seekBar = (SeekBar) v.findViewById(R.id.seekBar);
            alertDialogBuilder.setView(v);
            seekBar.setProgress(value);
            seekValue = value;
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    seekValue = progress + 1;
                    ((TextView) v.findViewById(R.id.test)).setText("" + seekValue);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ILineThickness.onSuccess(seekValue);
                    dialog.dismiss();
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ILineThickness.onCancel();
                    dialog.dismiss();
                }
            });

            alertDialogBuilder.setOnKeyListener(new OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        ILineThickness.onCancel();
                        dialog.dismiss();
                    }
                    return false;
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            // create alert // dialog
            alertDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void showNewDoodler(Context context, final newDoodleListener doodleListener) {
        try {
            AlertDialog.Builder alertDialogBuilder = getBuilder(context);
            final View v = LayoutInflater.from(context).inflate(R.layout.layout_new_doodle, null, false);
            Spinner spnNoOfDoodler = (Spinner) v.findViewById(R.id.spnNoOfDoodler);
            Spinner spnTime = (Spinner) v.findViewById(R.id.spnTime);

            ((RadioGroup) v.findViewById(R.id.rgSize)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rd640) {
                        size = 640;
                    } else if (checkedId == R.id.rd1280) {
                        size = 1280;
                    }
                }
            });

            ((RadioGroup) v.findViewById(R.id.rgvisibitlity)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rdPublic) {
                        visibility = "Public";
                    } else if (checkedId == R.id.rdPrivate) {
                        visibility = "Private";
                    }
                }
            });

            ArrayAdapter<CharSequence> adapterNoOfDoodler = ArrayAdapter.createFromResource(context, R.array.noOfDoodles, android.R.layout.simple_spinner_item);
            ArrayAdapter<CharSequence> adapterTime = ArrayAdapter.createFromResource(context, R.array.timeLimit, android.R.layout.simple_spinner_item);

            adapterNoOfDoodler.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            adapterTime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spnNoOfDoodler.setAdapter(adapterNoOfDoodler);
            spnTime.setAdapter(adapterTime);

            spnNoOfDoodler.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    numOfDoodlers = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spnTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    time = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            alertDialogBuilder.setView(v);

            alertDialogBuilder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    String name = ((EditText) v.findViewById(R.id.edtNewDoodlerName)).getText().toString().trim();
                    NewDoodleModel data = new NewDoodleModel();
                    data.setName(!TextUtils.isEmpty(name) ? name : "");
                    data.setNoOfDoodles(numOfDoodlers);
                    data.setSize(size);
                    data.setTimeLimite(time);
                    data.setVisibility(visibility);
                    if (doodleListener != null)
                        doodleListener.onSuccess(data);
                    dialog.dismiss();
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (doodleListener != null)
                        doodleListener.onCancel();
                    dialog.dismiss();
                }
            });

            alertDialogBuilder.setOnKeyListener(new OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (doodleListener != null)
                            doodleListener.onCancel();
                        dialog.dismiss();
                    }
                    return false;
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            // create alert // dialog
            alertDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showColourPicker(Activity context, int colour, final IColourListner listner) {
        try {
            AlertDialog.Builder alertDialogBuilder = getBuilder(context);
            final View v = LayoutInflater.from(context).inflate(R.layout.layout_colour_picker, null, false);

            final LobsterPicker lobsterPicker = (LobsterPicker) v.findViewById(R.id.lobsterpicker);
            lobsterPicker.setHistory(colour);
            lobsterPicker.setColorHistoryEnabled(true);

            lobsterPicker.addOnColorListener(new OnColorListener() {
                @Override
                public void onColorChanged(@ColorInt int color) {
                    v.findViewById(R.id.clrView).setBackgroundColor(color);
                }

                @Override
                public void onColorSelected(@ColorInt int color) {
                }
            });

            alertDialogBuilder.setView(v);

            alertDialogBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    listner.onAccept(lobsterPicker.getColor());
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listner.onCancel();
                    dialog.dismiss();
                }
            });

            alertDialogBuilder.setOnKeyListener(new OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        listner.onCancel();
                        dialog.dismiss();
                    }
                    return false;
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            // create alert // dialog
            alertDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @SuppressLint("NewApi")
    private static AlertDialog.Builder getBuilder(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(false);
        return alertDialogBuilder;
    }
}
